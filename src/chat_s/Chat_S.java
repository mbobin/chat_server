/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat_s;

/**
 *
 * @author marius
 */
// Unitatea de compilare Chat_S.java

import java.net.*;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Chat_S {

    static Resurse c;

    Chat_S() throws Exception {

        c = new Resurse();
        ServerSocket ss = null;
        Socket cs = null;
        Scanner sc = new Scanner(System.in);

        System.out.print("Portul : ");
        ss = new ServerSocket(sc.nextInt());
        System.out.println("Serverul a pornit !");

        while (true) {

            cs = ss.accept();

            DataOutputStream out = new DataOutputStream(cs.getOutputStream());
            DataInputStream in = new DataInputStream(cs.getInputStream());
            boolean conectat = false;
            String nume = in.readUTF();

            conectat = c.adaugaClient(nume, out);
            
            if (conectat) {
                System.out.println("\n Un client nou s-a conectat: \t" + nume);
                out.writeUTF("acceptat");
                out.writeUTF("Conectare reusita. Salut, " + nume
                        + "!\nTastati help pentru a vedea comenzile disponibile.");

                new ServerThread(c, cs, nume);
            } else {
                out.writeUTF("refuzat");
                cs.close();
                in.close();
                out.close();

            }

        }
    }

    public static void main(String[] args) throws Exception {

        Chat_S srv = new Chat_S();
    }
}

class ServerThread extends Thread {

    Socket cs = null;
    DataInputStream dis = null;
    DataOutputStream dos = null;
    String nume;
    static Resurse A;

    ServerThread(Resurse resursa, Socket socket, String id) throws IOException {

        A = resursa;
        cs = socket;
        dis = new DataInputStream(cs.getInputStream());
        dos = Resurse.clientiOut.get(id);
        nume = id;
        start();
    }

    @Override
    public void run() {

        try {
            while (true) {
                String message = dis.readUTF();
                String comanda;

                if (!message.isEmpty()) {
                    switch (message) {
                        case "list":

                            A.listClienti(dos);

                            break;
                        case "quit":

                            if (!cs.isClosed()) {
                                cs.close();

                                dis.close();
                                A.quit(nume);
                            }
                            stop();
                            break;
                        default:
                            Scanner sc_cuv = new Scanner(message).useDelimiter("[ ]");
                            comanda = sc_cuv.next();

                            switch (comanda) {
                                case "bcast":
                                    if (sc_cuv.hasNext()) {
                                        A.broadcast(nume, sc_cuv.nextLine());
                                    } else {
                                        A.msg("Server: Eroare: ", nume,
                                                "\n\tMesajul nu a fost introdus. Ex: msg mesaj");
                                    }
                                    break;

                                case "nick":
                                    if (!sc_cuv.hasNext()) {
                                        A.msg("Server: Eroare ", nume, "\n\tNumele nou nu a fost definit. "
                                                + "Ex: nick nume_nou\n");
                                    } else {
                                        nume = A.schimbaNume(nume, sc_cuv.next());
                                    }
                                    break;

                                case "msg":
                                    if (!sc_cuv.hasNext()) {
                                        A.msg("Server: Eroare ", nume, "\n\tDestinatarul nu a fost specificat. "
                                                + "Ex: msg destinatar mesaj\n");
                                    } else {
                                        String dest = sc_cuv.next();
                                        if (!sc_cuv.hasNext()) {
                                            A.msg("Server: Eroare ", nume, "\n\tMesajul nu a fost introdus."
                                                    + " Ex: msg destinatar mesaj\n");
                                        } else {
                                            A.msg(nume, dest, sc_cuv.nextLine());
                                        }
                                    }
                                    break;

                                default:
                                    A.msg("Server: Help: ", nume, "\n\tlist \t\t  Afiseaza toti clientii logati."
                                            + "\n\tmsg client mesaj  Trimite mesajul clientului specificat."
                                            + "\n\tbcast mesaj \t  Trimite mesajul tuturor clientilor conectati."
                                            + "\n\tnick nume_nou \t  Schimba numele cu cel specificat."
                                            + "\n\tquit \t\t  Logout. ");
                            }
                    }
                }
            }
        } catch (IOException e) {
            A.quit(nume);
        }
    }
}

class Resurse {

    static HashMap<String, DataOutputStream> clientiOut;

    Resurse() {
        clientiOut = new HashMap();
    }

    boolean numeValid(String nume) {

        String numeIlegale[] = {"list", "msg", "bcast", "nick", "quit", ""};

        for (String i : numeIlegale) {
            if (nume.equals(i)) {
                return false;
            }
        }
        return true;
    }

    synchronized boolean adaugaClient(String nume, DataOutputStream out) {

        boolean numeDisponibil;

        numeDisponibil = !clientiOut.containsKey(nume);

        if (!(numeValid(nume) && numeDisponibil)) {
            return false;
        }

        clientiOut.put(nume, out);
        return true;

    }

    synchronized void listClienti(DataOutputStream cl) {
        
        try {
            cl.writeUTF("Clientii conectati sunt: \n");
        } catch (IOException ex) {
            Logger.getLogger(Resurse.class.getName()).log(Level.SEVERE, null, ex);
        }
        String[] lista = new String[clientiOut.size()];
        lista = clientiOut.keySet().toArray(lista);
        for (String i : lista) {
            try {
                cl.writeUTF("\t" + i);
            } catch (IOException ex) {
                Logger.getLogger(Resurse.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    synchronized void broadcast(String id, String mesaj) {

        for (DataOutputStream i : clientiOut.values()) {
            try {
                i.writeUTF(id + " => toti: " + mesaj);
            } catch (IOException ex) {
                Logger.getLogger(Resurse.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    synchronized String schimbaNume(String numeVechi, String numeNou) {

        if (clientiOut.containsKey(numeNou) || numeValid(numeNou) == false) {
            try {
                clientiOut.get(numeVechi).writeUTF("Numele nu este valid. Incercati din nou.");
            } catch (IOException ex) {
                Logger.getLogger(Resurse.class.getName()).log(Level.SEVERE, null, ex);
            }
            return numeVechi;
        } else {
            DataOutputStream out = clientiOut.get(numeVechi);
            clientiOut.remove(numeVechi);
            clientiOut.put(numeNou, out);
            try {
                clientiOut.get(numeNou).writeUTF("Numele a fost schimbat cu succes!\n");
            } catch (IOException ex) {
                Logger.getLogger(Resurse.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("\n Clientul " + numeVechi + " este acum " + numeNou + ".");
            return numeNou;
        }

    }

    synchronized void quit(String nume) {
        try {
            clientiOut.get(nume).close();
        } catch (IOException ex) {
            Logger.getLogger(Resurse.class.getName()).log(Level.SEVERE, null, ex);
        }
        clientiOut.remove(nume);
        System.out.println("\n Un client s-a deconectat: \t" + nume);
    }

    void msg(String id1, String id2, String mesaj) {

        if (clientiOut.containsKey(id2)) {
            try {
                clientiOut.get(id2).writeUTF(id1 + " => " + id2 + " : " + mesaj);
            } catch (IOException ex) {
                Logger.getLogger(Resurse.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                clientiOut.get(id1).writeUTF("Server: Eroare: "
                        + "\n\tDestinatarul mesajului nu este logat. "
                        + "\n\tPentru a vedea clientii logati folositi comanda list.");
            } catch (IOException ex) {
                Logger.getLogger(Resurse.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
